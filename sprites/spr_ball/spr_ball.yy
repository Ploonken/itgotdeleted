{
    "id": "63657dd1-2cbd-4f1c-8c13-7a0749e07d24",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ball",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f80eda66-26b8-4803-8a64-6c29750f2ad3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63657dd1-2cbd-4f1c-8c13-7a0749e07d24",
            "compositeImage": {
                "id": "36985cd6-1f70-48a5-9f3f-01304a32079d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f80eda66-26b8-4803-8a64-6c29750f2ad3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f57ab7c-6749-444d-97bd-934ed95abb32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f80eda66-26b8-4803-8a64-6c29750f2ad3",
                    "LayerId": "2456bdcf-b4ef-4c38-8a0b-70407ead0a6f"
                },
                {
                    "id": "707ab73d-2dad-42d6-bb26-3f42411d2806",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f80eda66-26b8-4803-8a64-6c29750f2ad3",
                    "LayerId": "3c0d1f8c-04cb-420f-9d95-b74ddd9ec733"
                },
                {
                    "id": "50a15a3c-7d23-4ab9-b47a-1221d5f0f13c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f80eda66-26b8-4803-8a64-6c29750f2ad3",
                    "LayerId": "a9a2a57f-9e1f-4387-ba29-fab6201a271a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "2456bdcf-b4ef-4c38-8a0b-70407ead0a6f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "63657dd1-2cbd-4f1c-8c13-7a0749e07d24",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "a9a2a57f-9e1f-4387-ba29-fab6201a271a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "63657dd1-2cbd-4f1c-8c13-7a0749e07d24",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "3c0d1f8c-04cb-420f-9d95-b74ddd9ec733",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "63657dd1-2cbd-4f1c-8c13-7a0749e07d24",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2 (2)",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}